# Definiens

Documentation regarding the tasks requested
- Provisions the fully functional VM running any major Linux distribution of choice
- Sets up docker and run a web server docker container on the VM (let it be Apache, Nginx or anything else you feel comfortable with) which serves either simple static web page (but not the default web server landing page), or even web application of your choice
- Create a check if the web application is available and running fine
- If possible, configure the firewall to allow only access to SSH and web server ports

## AWS Cloud 

For the provisioning of the instance in EC2 will be using the `Terraform tool`. For other things like package installation, configurations, and tests will be executed by `Ansible`. 


## OS supported

- **CentOS 7 x64**

## Requirements
- Ansible v2.7 
- Goss v0.3.6
- Terraform v0.11.11 


# Terraform 

AWS credentials are required as environment variables. 

```bash
$ export AWS_ACCESS_KEY_ID=<KEY_ID>
$ export AWS_SECRET_ACCESS_KEY=<SECRET_ACCESS_KEY>
```


### Variables 

All variables can be viewed/overwritten in the file [variables.tf](variables.tf).

| Name           | Description  | Value                      |
| -------------- | -------------| ---------------------------|
| `aws_region` | Specific AWS region |  us-east-1            |
| `aws_tag` |  Name of instance |  webserver                 |
| `aws_ami` |ID of a registered AMI | ami-0199893068f89a449  |
| `aws_instance_type` | The type of the Instance |  t2.micro |
| `key_name` | The key pair name |  aws_ec2_us_east |
| `vpc_id`      | ID VPC |   vpc-7e17a674               |
| `ssh_key_private` | Path private key  | ~/.aws/priv_key.pem   |


The ansible playbook will be run by `local-exec provisioner` invokes a local executable after a resource is created. <br>

> ["Note that even though the resource will be fully created when the provisioner is run, there is no guarantee that it will be in an operable state - for example system services such as sshd may not be started yet on compute resources."](https://www.terraform.io/docs/provisioners/local-exec.html) 

<br>
One workaround was to include the `remote-exec`. This module performs connection attempt by ssh in the instance to execute the command *"echo just checking for ssh .ttyl. Bye."* <br>

The next command to be executed will be the playbook ansible by the module `local-exec`. <br>


## How to run

```sh 
$ terraform init 
$ terraform plan 
$ terraform apply 
$ terraform destroy 
```

 
# Ansible 

The [ansible directory](ansible) contains the playbook and roles for installation, configuration and testing. <br>

### Variables 

All variables can be viewed/overwritten in the file [main.yml](ansible/var/main.yml).

| Name           | Description  | Value                      |
| -------------- | -------------| ---------------------------|
| `docker_version` |  Docker-ce version for install |  18.09.1            |
| `docker_compose_version` |  Docker-compose version for install  | 1.18.0 |


### Roles

    - setup-centos
        Performs install "EPEL repository" and "yum upgrade"

    - setup-docker
        Performs the installation of "docker-ce" and "docker-compose".

    - setup-goss
        Performs the installation of "goss"


# Tests

The tests were divided into two environments.

## 1. Instance Setup 

Use the **goss tool** for testing.

### Role
    - setup_tests
        Run tests to check the version of packages, services and groups created.
    
## 2. Application

Uses **curl** and ansible **URI** module doe checking.

### Role
    - deploy_app_test
        Run testes to check if application is UP.
<br>

# Web-app

The web-app will run on a custom image based on nginx:1.15.8-alpine and will be build up at the time of the docker-compose up. <br>


> Custom image has been configured to use self-signed SSL certificate, redirect between HTTP to HTTPs and volume to persisting data.

<br>
<br>

# Maintainer

### Aloisio Bilck (aloisiobilck@gmail.com)
<br>
