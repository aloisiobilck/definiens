provider "aws" {
  region = "${var.aws_region}"
}
resource "random_id" "server" {
  byte_length = 8
}

resource "aws_instance" "instance" {
  ami = "${var.aws_ami}"
  instance_type = "${var.aws_instance_type}"
  security_groups = ["open_ssh_http"]
  key_name = "${var.key_name}"


  tags {
    Name = "${var.aws_tag}-${random_id.server.hex}"
  }

   
   provisioner "remote-exec" {
   inline = ["echo just checking for ssh. ttyl. bye."]

      connection {
        host        = "${self.public_ip}"
        type        = "ssh"                 
        private_key = "${file("${var.ssh_key_private}")}"
        user        = "centos"
      }
  }


    provisioner "local-exec" {
      command = "ansible-playbook -i '${self.public_ip},' --private-key ${var.ssh_key_private} ansible/setup.yml -u centos"
    }

}

