
variable "aws_region" {
  default = ""
  
}

variable "aws_tag" {
  description = "Name of instance"
  default = ""
}

variable "aws_ami" {
  default = ""
  
}
variable "aws_instance_type" {
  default = ""
  
}


variable "vpc_id" {
  default = ""
  
}

variable "ssh_key_private" {
  default = "~/.aws/priv_key.pem"
}

variable "key_name" {
  default = "aws_ec2_us_east"
}
